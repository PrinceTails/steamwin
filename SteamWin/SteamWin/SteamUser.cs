﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SteamWin
{
    public class SteamUser
    {
        public string communityID;
        public long id64;
        public string Name;
        public bool publicUser;
        public string urlAvatar;

        public SteamUser(string name)
        {
            if (name == "")
                return;
            string url = "http://steamcommunity.com/id/" + name + "/?xml=1";
            communityID = name;
            var reader = XmlReader.Create(url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });
            bool full = false;
            bool changedUrl = false;
            while (reader.Read() && !full)
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "error":
                                if (changedUrl)
                                {
                                    return;
                                }
                                reader = XmlReader.Create("http://steamcommunity.com/profiles/" + name + "/?xml=1");
                                changedUrl = true;
                                break;
                            case "steamID64":
                                reader.Read();
                                id64 = Convert.ToInt64(reader.Value);
                                break;
                            case "steamID":
                                reader.Read();
                                Name = reader.Value;
                                break;
                            case "visibilityState":
                                reader.Read();
                                if (reader.Value != "3")
                                {
                                    return;
                                }
                                break;
                        }
                        break;
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
