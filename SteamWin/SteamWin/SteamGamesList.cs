﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SteamWin
{
    public class SteamGamesList
    {
        public List<SteamGame> gamesList;

        

        public SteamGamesList()
        {
            gamesList = new List<SteamGame>();
        }

        //получение списка индексов приложений
        public List<FileInfo> getInstalledList(List<string> folders)
        {
            List<FileInfo> manifest_list = new List<FileInfo>();

            
            for (int j = 0; j < folders.Count; j++)
            {

                DirectoryInfo dir = new DirectoryInfo(folders[j]);
                //if (!dir.Exists)
                //    return null;
                FileInfo[] finfo = dir.GetFiles();
                for (int i = 0; i < finfo.GetLength(0); i++)
                {
                    string ch = Path.GetFileNameWithoutExtension(finfo[i].FullName);
                    if (ch.Contains("appmanifest"))
                        manifest_list.Add(finfo[i]);
                }
            }
            return manifest_list;
        }

        public void getInstalledSteamGames(List<FileInfo> manifest_list)
        {
            char[] split_char = { '"', '}', '{', '\n', '\t' };

            for (int i = 0; i < manifest_list.Count; i++)
            {
                int id = 0;
                string name = "";
                long size=0;

                byte[] buffer = new byte[256];

                //открытие и считывания данных из манифеста
                FileStream fs = manifest_list[i].OpenRead();
                fs.Read(buffer, 0, 256);
                fs.Close();

                string manifest_descriptor = Encoding.UTF8.GetString(buffer);
                string[] headers = manifest_descriptor.Split(split_char, StringSplitOptions.RemoveEmptyEntries);

                for (int j = 0; j < headers.Length; j++)
                {
                    if (id == 0 && headers[j] == "appid")
                    {
                        id = Convert.ToInt32(headers[j + 1]);
                        if (id == 228980)
                            break;
                        j++;
                        continue;
                    }
                    if (name == "" && headers[j] == "name")
                    {
                        name = headers[j + 1];
                    }
                    if(size==0 && headers[j] == "SizeOnDisk")
                    {
                        size = Convert.ToInt64(headers[j + 1]);
                        break;
                    }
                    if (j == headers.Length - 1 && name == "")
                        name = "Unknow Game";
                }
                if (id == 228980)
                    continue;
                gamesList.Add(new SteamGame(id,name,size));
            }
        }

        //сортировка списка игр по названию
        public List<SteamGame> sortGamesList(List<SteamGame> list)
        {
            List<SteamGame> sort = new List<SteamGame>();

            List<string> names = new List<string>();

            for (int i = 0; i < list.Count; i++)
            {
                names.Add(list[i].ToString());
            }
            names.Sort();

            for (int i = 0; i < list.Count; i++)
                for (int j = 0; j < list.Count; j++)
                {
                    if (names[i] == list[j].ToString())
                        sort.Add(list[j]);
                }
            return sort;
        }

        //WTF IS THIS DOING
        public void getGamesList(string url)
        {
            try
            {
                var reader = XmlReader.Create(url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "game":
                                    SteamGame tmp = new SteamGame();
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    tmp.Id = Convert.ToInt32(reader.Value);
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    tmp.Name = reader.Value;
                                    gamesList.Add(tmp);
                                    break;
                            }
                            break;

                        case XmlNodeType.EndElement:
                            if (reader.Name == "games")
                                return;
                            break;
                    }
                }
            }
            catch (Exception)
            {
                //throw new Exception("Error");
            }
        }
        public static List<SteamGame> getGamesList(SteamUser user)
        {
            try
            {
                var reader = XmlReader.Create(String.Format("http://steamcommunity.com/profiles/{0}/games?xml=1", user.id64), new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });
                List <SteamGame> output = new List<SteamGame>();
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "game":
                                    SteamGame tmp = new SteamGame();
                                    while (reader.Name != "appID")
                                        reader.Read();
                                    reader.Read();
                                    tmp.Id = Convert.ToInt32(reader.Value);
                                    while (reader.Name != "name")
                                        reader.Read();
                                    reader.Read();
                                    tmp.Name = reader.Value;
                                    while (reader.Name != "logo")
                                        reader.Read();
                                    reader.Read();
                                    tmp.LogoURL = reader.Value;
                                    output.Add(tmp);
                                    break;
                            }
                            break;

                        case XmlNodeType.EndElement:
                            if (reader.Name == "games")
                                return output;
                            break;
                    }
                }
                return output;
            }
            catch (Exception)
            {
                //throw new Exception("Error");
                return null;
            }
        }

        public List<SteamGame> filterList(List<int> filterindex)
        {
            List<SteamGame> dst = new List<SteamGame>();
            string[] tmp = new string[filterindex.Count];
            for (int i=0;i<tmp.GetLength(0);i++)
            {
                tmp[i] = SteamDB.getNameCategory(filterindex[i]);
            }
            for(int i=0;i<gamesList.Count;i++)
            {
                if (gamesList[i].filterGame(tmp))
                    dst.Add(gamesList[i]);
            }
            return dst;
        }

        public List<SteamGame> filterList(string [] filterstrings)
        {
            List<SteamGame> dst = new List<SteamGame>();
            for (int i = 0; i < gamesList.Count; i++)
            {
                if (gamesList[i].filterGame(filterstrings))
                    dst.Add(gamesList[i]);
            }
            return dst;
        }

        public void getType()
        {
            for (int i = 0; i < gamesList.Count; i++)
                gamesList[i].ApplicationType = SteamDB.getType(gamesList[i].Id);
        }
        public void getCategory()
        {
            for (int i = 0; i < gamesList.Count; i++)
                gamesList[i].ApplicationCategory = SteamDB.getCategory(gamesList[i].Id);
        }
        public void getHLTB()
        {
            for (int i = 0; i < gamesList.Count; i++)
                gamesList[i].GetHLTBInfo();
        }
    }
}
