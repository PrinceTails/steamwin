﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamWin
{
    public class SteamApp
    {
        public List<string> LibraryFolders;
        public string SteamPath;
        public double SteamVersion;

        public SteamApp()
        {
            SteamPath = SteamApp.getSteamPath();
            LibraryFolders = getLibraryFolders();
        }

        public static string getSteamPath()
        {
            if (Environment.Is64BitOperatingSystem)
                return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Steam";
            else
                return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Steam";
        }

        private List<string> getLibraryFolders()
        {
            List<string> folders = new List<string>();
            char[] split_char = { '"', '}', '{', '\n', '\t' };
            StreamReader stream = new StreamReader(this.SteamPath + @"\steamapps\libraryfolders.vdf");
            string text = stream.ReadToEnd();
            var lf = text.Split(split_char, StringSplitOptions.RemoveEmptyEntries);
            lf[4] = this.SteamPath;
            for (int j = 4; j < lf.GetLength(0); j += 2)
            {
                if (Directory.Exists(lf[j] + @"\SteamApps"))
                    folders.Add(lf[j] + @"\SteamApps");
            }
            return folders;
        }


    }
}
