﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SteamWin
{
    public class SteamDB
    {
        enum Category
        {
            All = 0, MP, SP, Mods_HL2 = 6, Mods_HL1, VAC, Coop, demo,
            Captions_available = 13, Commentary_available, Stats, Includes_SDK,
            level_editor, Partial_Controller, Mods, MMO, DLC,
            Steam_Achievements, Steam_Cloud, Shared_Split_Screen, Steam_Leaderboards,
            Cross_Platform_MP = 27, Full_controller, Steam_Trading_Cards, Steam_Workshop,
            VR, Steam_Turn_Notifications, InApp_Purchases = 35, Online_MP, Local_MP,
            Online_Coop, Local_Coop, HTC_Vive = 101, Oculus_Rift, OSVR, Early_Access = 666
        }
        
        public static string getUrlIcon(int id)
        {
            int cur = 0;
            int readed = 0;
            string resp = "";
            string webSite = String.Format(@"http://steamdb.info/app/{0}/info", id);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webSite);
            request.UserAgent = "SteamDB-api by Prince Tails";
            request.Accept = "text/html";
            HttpWebResponse wRes = (HttpWebResponse)request.GetResponse();
            var da = wRes.GetResponseStream();

            do
            {
                byte[] buf = new byte[1024];
                readed = da.Read(buf, 0, 1024);
                cur += readed;
                resp += Encoding.UTF8.GetString(buf, 0, buf.Length);
            }
            while (cur <= 40000);
            wRes.Close();
            string regex = "href\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
            MatchCollection match = Regex.Matches(resp, regex);
            foreach (Match m in match)
            {
                string link = m.Groups[1].Value;
                if (link.Contains("https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/"))
                {
                    string istrue = link.Substring(link.Length - 3);
                    if (istrue == "ico")
                    {
                        return link;
                    }
                }
            }
            return "noicon";
        }

        static void downloadIcon(string link, string path)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.DownloadFile(link, String.Format(path + @"\{0}.ico", link.GetHashCode()));
                    client.Dispose();
                }
                catch
                {

                }
            }
        }
        public static List<string> getCategory(int id)
        {
            List<string> category = new List<string>();
            int cur = 0;
            int readed = 0;
            string resp = "";
            string webSite = String.Format(@"http://steamdb.info/app/{0}/info", id);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webSite);
            request.UserAgent = "SteamDB-api by Prince Tails";
            request.Accept = "text/html";
            HttpWebResponse wRes = (HttpWebResponse)request.GetResponse();

            var da = wRes.GetResponseStream();
            do
            {
                byte[] buf = new byte[600];
                readed = da.Read(buf, 0, 600);
                cur += readed;
                //if (cur > 8000)
                    resp += Encoding.UTF8.GetString(buf,0,readed);
            }
            while (cur <= 16500);
            wRes.Close();

            string regex = "aria-label\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
            MatchCollection match = Regex.Matches(resp, regex);
            for(int i=0;i<match.Count;i++)
            {
                if(match[i].Groups[1].Value.Contains("SteamDB Rating"))
                {
                    for (int j = i + 1; j < match.Count; j++)
                        category.Add(match[j].Groups[1].Value);
                    break;
                }
            }

            return category;
        }
        public static string getType(int id)
        {
            int cur = 0;
            int readed = 0;
            string resp = "";
            string webSite = String.Format(@"http://steamdb.info/app/{0}/info", id);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webSite);
            request.UserAgent = "SteamDB-api by Prince Tails";
            request.Accept = "text/html";
            HttpWebResponse wRes = (HttpWebResponse)request.GetResponse();

            var da = wRes.GetResponseStream();
            do
            {
                byte[] buf = new byte[600];
                readed = da.Read(buf, 0, 600);
                cur += readed;
                if (cur > 8000)
                    resp += Encoding.UTF8.GetString(buf, 0, buf.Length);
            }
            while (cur <= 16500);
            wRes.Close();
            string regex2 = "\"applicationCategory\">(.*?)</td>";
            MatchCollection match2 = Regex.Matches(resp, regex2);
            if (match2.Count == 0)
                return "Unknowned";
            return match2[0].Groups[1].Value;
        }
        public static string getNameCategory(int cat)
        {
            switch (cat)
            {
                case 0:
                    return "All";
                case 1:
                    return "Multi-player";
                case 2:
                    return "Single-player";
                case 6:
                    return "Mods (require HL2)";
                case 7:
                    return "Mods (require HL1)";
                case 8:
                    return "Valve Anti-Cheat enabled";
                case 9:
                    return "Co-op";
                case 10:
                    return "Game demo";
                case 13:
                    return "Captions available";
                case 14:
                    return "Commentary available";
                case 15:
                    return "Stats";
                case 16:
                    return "Includes Source SDK";
                case 17:
                    return "Includes level editor";
                case 18:
                    return "Partial Controller Support";
                case 19:
                    return "Mods";
                case 20:
                    return "MMO";
                case 21:
                    return "Downloadable Content";
                case 22:
                    return "Steam Achievements";
                case 23:
                    return "Steam Cloud";
                case 24:
                    return "Shared/Split Screen";
                case 25:
                    return "Steam Leaderboards";
                case 27:
                    return "Cross-Platform Multiplayer";
                case 28:
                    return "Full controller support";
                case 29:
                    return "Steam Trading Cards";
                case 30:
                    return "Steam Workshop";
                case 31:
                    return "VR Support";
                case 32:
                    return "Steam Turn Notifications";
                case 35:
                    return "In-App Purchases";
                case 36:
                    return "Online Multi-Player";
                case 37:
                    return "Local Multi-Player";
                case 38:
                    return "Online Co-op";
                case 39:
                    return "Local Co-op";
                case 101:
                    return "HTC Vive";
                case 102:
                    return "Oculus Rift";
                case 103:
                    return "OSVR";
                case 666:
                    return "Early Access";
                default:
                    return "No category";
            }
        }

    }
}
