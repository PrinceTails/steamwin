﻿using System;
using System.Text;
using AngleSharp.Parser.Html;
using System.Net;
using System.IO;

namespace SteamWin
{
    public class HLTB
    {

        #region переменные класа
        static readonly string searchPattern = "https://howlongtobeat.com/search_results.php?";
        static readonly string gameLinkPattern = "https://howlongtobeat.com/game.php?id=";
        static readonly int countIterates = 2;

        static char[] splitChar = { ':', '-', ' ', '™', '®', '©' , 'Û', 'û' };
        static string[] replaceString = { "Director’s Cut", "Director's Cut", "Game of the Year Edition" };

        private string originalName;
        private string convertedName;

        public string Name { get; private set; }
        public string Id { get; private set; }
        public string Main { get; private set; }
        public string MainExtra { get; private set; }
        public string Completionist { get; private set; }

        public string GameLink { get; private set; }
        #endregion
        public HLTB(string name, string id, string main, string mainextra, string completionist, string gamelink)
        {
            Name = name;
            Id = id;
            Main = main;
            MainExtra = mainextra;
            Completionist = completionist;
            GameLink = gamelink;
        }

        public HLTB(string gameName)
        {
            this.originalName = gameName;
            this.convertedName = this.ParseGame();
            ParseData(GetData());
        }

        private string ParseGame()
        {
            string tmp = this.originalName;
            string ret = "";
            string[] splitted = tmp.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < splitted.Length; i++)
                ret += splitted[i] + " ";
            ret = ret.Remove(ret.Length - 1).ToLower();
            return ret;
        }

        private string GetData()
        {
            string respString = "";
            for (int iter = 0; iter < HLTB.countIterates; iter++)
            {
                WebRequest req = WebRequest.Create(HLTB.searchPattern);
                req.Method = "POST";
                string post = "queryString=" + this.convertedName;
                byte[] bytearray = Encoding.UTF8.GetBytes(post);
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = bytearray.Length;
                Stream dataStream = req.GetRequestStream();
                dataStream.Write(bytearray, 0, bytearray.Length);
                dataStream.Close();
                WebResponse resp = req.GetResponse();
                dataStream = resp.GetResponseStream();
                StreamReader read = new StreamReader(dataStream);
                respString = read.ReadToEnd();
                read.Close();
                dataStream.Close();
                resp.Close();

                if (respString.Contains("No results for") && iter == HLTB.countIterates - 1)
                    respString = "No Information";

                if (respString.Contains("No results for"))
                {
                    if (convertedName.EndsWith("edition"))
                    {
                        string[] tmp = convertedName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        this.convertedName = "";
                        for (int i = 0; i < tmp.Length - 2; i++)
                        {
                            this.convertedName += tmp[i] + " ";
                        }
                        this.convertedName.Remove(this.convertedName.Length - 1);
                    }
                    for (int i = 0; i < replaceString.Length; i++)
                        this.convertedName = this.convertedName.Replace(replaceString[i].ToLower(), "");
                }
                else
                    break;
            }
            return respString;

        }

        private void ParseData(string text)
        {
            if (text == "No Information")
            {
                this.Id = "Unknown";
                this.Name = "Unknown";
                this.Main = "Unknown";
                this.MainExtra = "Unknown";
                this.Completionist = "Unknown";
                this.GameLink = "Unknown";
                return;
            }

            HtmlParser parser = new HtmlParser();
            var document = parser.Parse(text);

            var selector = document.QuerySelector("div.search_list_details");
            if(selector==null)
            {
                this.Id = "Unknown";
                this.Name = "Unknown";
                this.Main = "Unknown";
                this.MainExtra = "Unknown";
                this.Completionist = "Unknown";
                this.GameLink = "Unknown";
                return;
            }
            string[] content = selector.Children[1].TextContent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            this.Name = selector.Children[0].TextContent.Replace("\n", "");
            this.Id = selector.Children[0].Children[0].GetAttribute("Href").Replace("game.php?id=", "");
            this.GameLink = HLTB.gameLinkPattern + this.Id;
            for (int i = 0; i < content.Length; i++)
            {
                if (content[i] == " ")
                    continue;
                switch (content[i])
                {
                    case "Main Story":
                        {
                            this.Main = content[i + 1];
                            i++;
                            break;
                        }
                    case "Main + Extra":
                        {
                            this.MainExtra = content[i + 1];
                            i++;
                            break;
                        }
                    case "Completionist":
                        {
                            this.Completionist = content[i + 1];
                            i++;
                            break;
                        }
                }
            }


        }
    }
}
