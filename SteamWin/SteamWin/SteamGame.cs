﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamWin
{
    public class SteamGame
    {
        public int Id;
        public string Name;
        public string IconURL;
        public string LogoURL;
        public string ApplicationType;
        public long SizeOnDisk;
        public List<string> ApplicationCategory;
        public HLTB HowLongToBeat;

        public string SizeOnDiskString
        {
            get
            {
                double tmp = ((Convert.ToDouble(SizeOnDisk / 1024)) / 1024) / 1024;
                if (tmp < 1)
                {
                    tmp *= 1024;
                    return Math.Round(tmp).ToString() + " MB";
                }
                return Math.Round(tmp, 3).ToString() + " GB";
            }
        }

        public SteamGame()
        {
            ApplicationCategory = new List<string>();
        }
        public SteamGame(int id, string name, long size = 0, bool more=false, bool hltbInfo = false)
        {
            this.Id = id;
            this.Name = name;
            this.LogoURL = getLogo();
            this.SizeOnDisk = size;
            if (more)
            {
                this.IconURL = SteamDB.getUrlIcon(id);
                this.ApplicationCategory = SteamDB.getCategory(id);
                this.ApplicationType = SteamDB.getType(id);
            }
            if (hltbInfo)
                HowLongToBeat = new HLTB(this.Name);

        }

        static public void launchGame(int ID)
        {
            Process pr = new Process();
            pr.StartInfo.FileName = "steam://rungameid/" + ID;
            pr.Start();
        }

        public void GetHLTBInfo()
        {
            this.HowLongToBeat = new HLTB(this.Name);
        }

        static public bool isLaunched(string name)
        {
            Process[] procList = Process.GetProcesses();
            for (int i = 0; i < procList.GetLength(0); i++)
                if (procList[i].MainWindowTitle == name)
                {
                    return true;
                }
            return false;
        }

        private string getLogo()
        {
            return "https://steamcdn-a.akamaihd.net/steam/apps/"+this.Id+"/header.jpg";
        }

        public override string ToString()
        {
            return this.Name;
        }

        public bool filterGame(string [] filternames)
        {
            if (this.ApplicationCategory.Count == 0)
                return true;
            for(int i=0;i<filternames.GetLength(0);i++)
            {
                for(int j=0;j<this.ApplicationCategory.Count;j++)
                {
                    if (filternames[i] == this.ApplicationCategory[j])
                        return true;
                }
            }
            return false;
        }
    }
}
